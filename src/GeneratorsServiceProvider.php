<?php

namespace Fifteen\Generators;

use Illuminate\Support\ServiceProvider;
use Fifteen\Generators\Console\Commands\ApiControllerGeneratorCommand;
use Fifteen\Generators\Console\Commands\ApiResourceGeneratorCommand;
use Fifteen\Generators\Console\Commands\ApiRoutesGeneratorCommand;
use Fifteen\Generators\Console\Commands\ControllerGeneratorCommand;
use Fifteen\Generators\Console\Commands\FormRequestGeneratorCommand;
use Fifteen\Generators\Console\Commands\MigrationGeneratorCommand;
use Fifteen\Generators\Console\Commands\ModelGeneratorCommand;
use Fifteen\Generators\Console\Commands\PresenterGeneratorCommand;
use Fifteen\Generators\Console\Commands\RepositoryGeneratorCommand;
use Fifteen\Generators\Console\Commands\ResourceGeneratorCommand;
use Fifteen\Generators\Console\Commands\ResourceServiceGeneratorCommand;
use Fifteen\Generators\Console\Commands\RoutesGeneratorCommand;
use Fifteen\Generators\Console\Commands\SeederGeneratorCommand;
use Fifteen\Generators\Console\Commands\TestGeneratorCommand;
use Fifteen\Generators\Console\Commands\TransformerGeneratorCommand;
use Fifteen\Generators\Console\Commands\ViewGeneratorCommand;
use Fifteen\Generators\Console\Commands\ViewsGeneratorCommand;
use Fifteen\Generators\Console\Commands\AllGeneratorCommand;


class GeneratorsServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    /**
     * Booting
     */
    public function boot()
    {
        $this->publish();
    }

    /**
     * Register the commands
     *
     * @return void
     */
    public function register()
    {
        foreach([
            'ApiController',
            'ApiResource',
            'ApiRoutes',
            'Controller',
            'FormRequest',
            'Migration',
            'Model',
            'Presenter',
            'Repository',
            'Resource',
            'Routes',
            'Seeder',
            'Service',
            'Test',
            'Transformer',
            'Views',
            'All',
        ] as $command)
        {
            $this->{"register$command"}();
        }
    }

    private function publish()
    {
         $this->publishes([
            __DIR__ . '/templates/' => base_path('resources/generators/templates/'),
        ], 'templates');

        $this->publishes([
            __DIR__ . '/../www/config/fifteen-generators.php' => config_path('fifteen-generators.php'),
        ], 'config');

    }

    /**
     * Register the migration generator
     */
    protected function registerMigration()
    {
        $this->app->singleton('gen.migration', function($app)
        {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new MigrationGeneratorCommand($generator);
        });
        $this->commands('gen.migration');
    }

    /**
     * Register the model generator
     */
    protected function registerModel()
    {
        $this->app->singleton('gen.model', function($app)
        {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ModelGeneratorCommand($generator);
        });
        $this->commands('gen.model');
    }

    /**
     * Register the view generator
     */
    protected function registerViews()
    {
        $this->app->singleton('gen.view', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ViewGeneratorCommand($generator);
        });
        $this->commands('gen.view');

        $this->app->singleton('gen.views', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ViewsGeneratorCommand($generator);
        });
        $this->commands('gen.views');
    }

    /**
     * Register the controller generator
     */
    protected function registerController()
    {
        $this->app->singleton('gen.controller', function($app)
        {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ControllerGeneratorCommand($generator);
        });

        $this->commands('gen.controller');
    }

    /**
     * Register the repository generator
     */
    protected function registerRepository()
    {
        $this->app->singleton('gen.repository', function($app)
        {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new RepositoryGeneratorCommand($generator);
        });

        $this->commands('gen.repository');
    }

    /**
     * Register the generator
     */
    protected function registerResource()
    {
        $this->app->singleton('gen.resource', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ResourceGeneratorCommand($generator);
        });
        $this->commands('gen.resource');
    }

    /**
     * Register the generator
     */
    protected function registerService()
    {
        $this->app->singleton('gen.service', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ResourceServiceGeneratorCommand($generator);
        });
        $this->commands('gen.service');
    }

    /**
     * Register the generator
     */
    protected function registerPresenter()
    {
        $this->app->singleton('gen.presenter', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new PresenterGeneratorCommand($generator);
        });
        $this->commands('gen.presenter');
    }

    /**
     * Register the generator
     */
    protected function registerRoutes()
    {
        $this->app->singleton('gen.routes', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new RoutesGeneratorCommand($generator);
        });
        $this->commands('gen.routes');
    }

    /**
     * Register the generator
     */
    protected function registerApiRoutes()
    {
        $this->app->singleton('gen.api-routes', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ApiRoutesGeneratorCommand($generator);
        });
        $this->commands('gen.api-routes');
    }

    /**
     * Register the generator
     */
    protected function registerTest()
    {
        $this->app->singleton('gen.test', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new TestGeneratorCommand($generator);
        });
        $this->commands('gen.test');
    }

    /**
     * Register the generator
     */
    protected function registerTransformer()
    {
        $this->app->singleton('gen.transformer', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new TransformerGeneratorCommand($generator);
        });
        $this->commands('gen.transformer');
    }

    /**
     * Register the generator
     */
    protected function registerFormRequest()
    {
        $this->app->singleton('gen.form-request', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new FormRequestGeneratorCommand($generator);
        });
        $this->commands('gen.form-request');
    }

    /**
     * Register the generator
     */
    protected function registerApiController()
    {
        $this->app->singleton('gen.api-controller', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ApiControllerGeneratorCommand($generator);
        });
        $this->commands('gen.api-controller');
    }

    /**
     * Register the generator
     */
    protected function registerApiResource()
    {
        $this->app->singleton('gen.api-resource', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new ApiResourceGeneratorCommand($generator);
        });
        $this->commands('gen.api-resource');
    }
    /**
     * Register the seeder generator
     */
    protected function registerSeeder()
    {
        $this->app->singleton('gen.seeder', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new SeederGeneratorCommand($generator);
        });
        $this->commands('gen.seeder');
    }
    /**
     * Register the generator
     */
    protected function registerAll()
    {
        $this->app->singleton('gen.all', function($app) {
            $generator = $this->app->make('Fifteen\Generators\Generator');
            return new AllGeneratorCommand($generator);
        });
        $this->commands('gen.all');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

}
