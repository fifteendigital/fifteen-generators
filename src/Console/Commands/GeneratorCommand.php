<?php namespace Fifteen\Generators\Console\Commands;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Console\Command;
use Fifteen\Generators\Filesystem\FileAlreadyExists;
use Fifteen\Generators\Generator;
use Fifteen\Generators\Support\Schema;
use Fifteen\Generators\Compilers\TemplateCompiler;
use Config;
use Illuminate\Support\Str;
use File;

use Illuminate\FileSystem\FileSystem;

abstract class GeneratorCommand extends Command {

	/**
	 * @var \Fifteen\Generators\ModelGenerator
	 */
	protected $generator;

	protected $schema;
	protected $basename;
	protected $options;

	/**
	 * @param Generator $generator
	 */
	public function __construct(Generator $generator)
	{
		$this->generator = $generator;
		parent::__construct();
	}

	protected abstract function create();

	/**
	 * Compile and generate the file
	 */
	public function fire()
	{

		$this->basename = $this->argument('basename');
		foreach (['delete', 'overwrite', 'force', 'path', 'subfolder', 'templatePath'] as $option) {
			$this->options[$option] = empty($this->option($option)) ? null : $this->option($option);
		}

		$schema = $this->getSchema($this->basename, true);
		if (empty($schema)) {
			$this->error($this->basename . '.json does not exist or is invalid.');
			die();
		} else {
			$this->parseSchema($this->basename);
		}
		$this->create();
	}

	/**
	 * Get a directory path either through a
	 * command option, or from the configuration
	 *
	 * @param $option
	 * @param $configName
	 * @return string
	 */
	protected function getPathByOptionOrConfig($option, $configName)
	{
		if ($path = $this->option($option))
		{
			return $path;
		}

		return Config::get("generators::config.{$configName}");
	}

	public function parseSchema($schema)
	{
		$items = $this->getSchema($schema);
		// pd($items);
		$errors = 0;
		if ( ! isset($items->fields) || ! is_object($items->fields)) {
			$errors++;
		}
		if ($errors) {
			$this->error("The schema is invalid. Please check and try again.");
			die();
		}
		$this->schema = new Schema($this->basename, $items);
	}

	public function getSchema($schema, $check = false)
	{
		$filesystem = new FileSystem();
		$file = $schema . '.json';
		$base_path = base_path('/database/schemas');
		$path = $base_path . '/' . $file;
		if ($filesystem->exists($path)) {
			$contents = $filesystem->get($path);
			return json_decode($contents);
		} else {
			if ($check) {
				return false;
			} else {
				$this->error("I can't find a schema called '{$file}'. Please check and try again.");
				die();
			}
		}	
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['overwrite', null, InputOption::VALUE_REQUIRED, 'Overwrite existing files?'],
			['delete', null, InputOption::VALUE_REQUIRED, 'Delete existing files?'],
			['force', null, InputOption::VALUE_REQUIRED, 'Perform actions without prompting?'],
			['subfolder', null, InputOption::VALUE_REQUIRED, 'Would you like to put the file within a subfolder?'],
			['path', null, InputOption::VALUE_REQUIRED, 'Where should the file be created?'],
			['templatePath', null, InputOption::VALUE_REQUIRED, 'The location of the template for this generator']
		];
	}

} 