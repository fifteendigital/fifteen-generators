<?php

namespace Fifteen\Generators\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ResourceGeneratorCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gen:resource';
    protected $basename;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new resource';
    protected $force = false;

    /**
     * Generate a resource
     *
     * @return mixed
     */
    public function fire()
    {
        $this->basename = $this->argument('basename');
        $this->force = $this->option('force');

        $this->callMigration();
        $this->callModel();
        $this->callRepository();
        $this->callTransformer();
        $this->callController();
        $this->callFormRequest();
        $this->callPresenter();
        $this->callService();
        $this->callViews();
        $this->callRoutes();
        // $this->callSeeder();
        // $this->callTest();
    }

    protected function getMessage($resource, $plurals = false)
    {
        if ($this->option('delete')) {
            $message = "Do you want me to delete the " . $resource . "?";
        } else {
            if ($plurals) {
                $article = '';
            } elseif ($this->beginsWithAVowel($resource)) {
                $article = 'an ';
            } else {
                $article = 'a ';
            }
            $message = "Do you want me to create " . $article . $resource . "?";
        }
        return $message;
    }

    protected function beginsWithAVowel($word)
    {

        $first_letter = strtolower(substr($word, 0, 1));
        return in_array($first_letter, ['a', 'e', 'i', 'o', 'u']);
    }

    /**
     * Call model generator if user confirms
     *
     * @param $basename
     */
    protected function callMigration()
    {
        if ($this->force || $this->confirm($this->getMessage('migration') . " [yes|no]"))
        {
            $this->call('gen:migration', $this->getData());
        }
    }

    /**
     * Call model generator if user confirms
     *
     * @param $basename
     */
    protected function callModel()
    {
        if ($this->force || $this->confirm($this->getMessage('model') . " [yes|no]"))
        {
            $this->call('gen:model', $this->getData());
        }
    }

    /**
     * Call view generator if user confirms
     *
     * @param $basename
     */
    protected function callViews()
    {
        if ($this->force || $this->confirm($this->getMessage('views', true) . " [yes|no]"))
        {
            $this->call('gen:views', $this->getData());
        }
    }

    /**
     * Call transformer generator if user confirms
     *
     * @param $basename
     */
    protected function callTransformer()
    {
        if ($this->force || $this->confirm($this->getMessage('transformer') . "[yes|no]"))
        {
            $this->call('gen:transformer', $this->getData());
        }
    }

    /**
     * Call controller generator if user confirms
     *
     * @param $basename
     */
    protected function callController()
    {
        if ($this->force || $this->confirm($this->getMessage('controller') . "[yes|no]"))
        {
            $this->call('gen:controller', $this->getData());
        }
    }

    /**
     * Call form request generator if user confirms
     *
     * @param $basename
     */
    protected function callFormRequest()
    {
        if ($this->force || $this->confirm($this->getMessage('form request') . " [yes|no]"))
        {
            $this->call('gen:form-request', $this->getData());
        }
    }

    /**
     * Call repository generator if user confirms
     *
     * @param $basename
     */
    protected function callRepository()
    {
        if ($this->force || $this->confirm($this->getMessage('repository') . " [yes|no]"))
        {
            $this->call('gen:repository', $this->getData());
        }
    }

    /**
    * Call presenter generator if user confirms
    *
    * @param $basename
    */
    protected function callPresenter()
    {
        if ($this->force || $this->confirm($this->getMessage('presenter') . " [yes|no]"))
        {
            $this->call('gen:presenter', $this->getData());
        }
    }

    /**
     * Call service generator if user confirms
     *
     * @param $basename
     */
    protected function callService()
    {
        if ($this->force || $this->confirm($this->getMessage('resource service') . " [yes|no]"))
        {
            $this->call('gen:resource-service', $this->getData());
        }
    }

    /**
     * Call route generator if user confirms
     *
     * @param $basename
     */
    protected function callRoutes()
    {
        if ($this->force || $this->confirm($this->getMessage('routes', true) . " [yes|no]"))
        {
            $this->call('gen:routes', $this->getData());
        }
    }

    /**
     * Call seeder generator if user confirms
     *
     * @param $basename
     */
    protected function callSeeder()
    {
        if ($this->force || $this->confirm($this->getMessage('seeds', true) . " [yes|no]"))
        {
            $this->call('gen:seed', $this->getData());
        }
    }

    /**
     * Call test generator if user confirms
     *
     * @param $basename
     */
    protected function callTest()
    {
        if ($this->force || $this->confirm($this->getMessage('tests', true) . " [yes|no]"))
        {
            $this->call('gen:test', $this->getData());
        }
    }

    public function getData()
    {
        $data = ['basename' => $this->basename];
        foreach (['schema', 'overwrite', 'delete', 'force'] as $item) {
            if ($this->option($item)) {
                $data['--' . $item] = $this->option($item);
            }
        }
        return $data;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['basename', InputArgument::REQUIRED, "The base name (e.g. 'customer address')."]
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['overwrite', null, InputOption::VALUE_REQUIRED, 'Overwrite existing files?'],
            ['delete', null, InputOption::VALUE_REQUIRED, 'Delete existing files?'],
            ['force', null, InputOption::VALUE_REQUIRED, 'Perform actions without prompting?'],
            ['schema', null, InputOption::VALUE_REQUIRED, 'Would you like the file to read from a schema?'],
            ['subfolder', null, InputOption::VALUE_REQUIRED, 'Would you like to put the file within a subfolder?'],
            ['path', null, InputOption::VALUE_REQUIRED, 'Where should the file be created?'],
            ['templatePath', null, InputOption::VALUE_REQUIRED, 'The location of the template for this generator']
        ];
    }

}
