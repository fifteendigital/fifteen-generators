<?php namespace Fifteen\Generators\Console\Commands;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Fifteen\Generators\FileRenderers\RepositoryFileRenderer as FileRenderer;

class RepositoryGeneratorCommand extends GeneratorCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gen:repository';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate a repository';

	/**
	 * Create file(s)
	 *
	 * @return mixed
	 */
	protected function create($schema = null)
	{
		if (!$schema) {
			$schema = $this->schema;
		}
		$renderer = new FileRenderer($this->generator);

        try {
            $messages = $renderer->create($schema, $this->options);
        } catch (Exception $e) {
            $this->error($e->message);
        }
		foreach ($messages as $message) {
			$this->info($message);
		}
		// Create any child files
		foreach ($schema->getChildren() as $child) {
			$this->create($child);
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['basename', InputArgument::REQUIRED, "The base name (e.g. 'customer address')."]
		];
	}

}
