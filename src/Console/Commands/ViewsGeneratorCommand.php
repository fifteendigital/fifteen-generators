<?php namespace Fifteen\Generators\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ViewsGeneratorCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gen:views';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate views';

	/**
	 * Generate views
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$basename = $this->argument('basename');
		foreach(['index', 'show', 'create', 'edit', 'sort', 'data', 'form', 'delete', 'filters', 'nested'] as $view) {
			$data = compact('basename', 'view');
			foreach (['schema', 'overwrite', 'delete', 'force'] as $item) {
				if ($this->option($item)) {
					$data['--' . $item] = $this->option($item);
				}
			}
			$this->call('gen:view', $data);
		}
		// // Create any child files
		// foreach ($this->schema->getChildren() as $child) {
		// 	$this->createNested($child);
		// }
	}

	// protected function createNested($schema)
	// {
	// 	$renderer = new FileRenderer($this->generator);

 //        try {
 //        	$options = $this->options;
 //        	$options['view'] = 'nested';
 //            $messages = $renderer->create($schema, $options);
 //        } catch (Exception $e) {
 //            $this->error($e->message);
 //        }
	// 	foreach ($messages as $message) {
	// 		$this->info($message);
	// 	}
	// 	foreach ($schema->gtChildren() as $child) {
	// 		$this->createNested($child);
	// 	}
	// }
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['basename', InputArgument::REQUIRED, "The base name (e.g. 'customer address')."]
		];
	}
	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['overwrite', null, InputOption::VALUE_REQUIRED, 'Overwrite existing files?'],
			['delete', null, InputOption::VALUE_REQUIRED, 'Delete existing files?'],
			['force', null, InputOption::VALUE_REQUIRED, 'Perform actions without prompting?'],
			['schema', null, InputOption::VALUE_REQUIRED, 'Would you like the file to read from a schema?'],
			['subfolder', null, InputOption::VALUE_REQUIRED, 'Would you like to put the file within a subfolder?'],
			['path', null, InputOption::VALUE_REQUIRED, 'Where should the file be created?'],
			['templatePath', null, InputOption::VALUE_REQUIRED, 'The location of the template for this generator']
		];
	}
}
