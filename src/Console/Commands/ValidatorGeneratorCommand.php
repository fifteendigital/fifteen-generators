<?php namespace Fifteen\Generators\Console\Commands;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Fifteen\Generators\FileRenderers\ValidatorFileRenderer as FileRenderer;

class ValidatorGeneratorCommand extends GeneratorCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gen:validator';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate a validator';

	/**
	 * Create file(s)
	 *
	 * @return mixed
	 */
	protected function create($schema = null)
	{
		if (!$schema) {
			$schema = $this->schema;
		}
		$renderer = new FileRenderer($this->generator);

        try {
            $messages = $renderer->create($schema, $this->options);
        } catch (Exception $e) {
            $this->error($e->message);
        }
		foreach ($messages as $message) {
			$this->info($message);
		}
		// Create any child files
		foreach ($schema->getChildren() as $child) {
			$this->create($child);
		}
	}

	/**
	 * The path where the file will be created
	 *
	 * @return mixed
	 */
	protected function getFileGenerationPath()
	{
		$names = $this->getNameStrings();
		$file_name = $names['studly_singular'] . 'Validator';
		$path = app_path() . '/Validators/Model';
		$subfolder = $this->option('subfolder');
		if ( ! empty($subfolder)) {
			$path .= '/' . $subfolder;
		}
		return $path . '/' . $file_name . '.php';
	}

	/**
	 * Fetch the template data
	 *
	 * @return array
	 */
	protected function getTemplateData($schema, $options = [])
	{
		$data = $schema->getNameStrings();

        $data['subfolder'] = $path = empty($options['subfolder']) ? '' : $options['subfolder'];

		$data['namespace'] = empty($data['subfolder']) ? '' : '\\' . $data['subfolder'];

		$data['vrules'] = $data['srules'] = '';
		foreach ($this->fields as $field) {
			if ( ! in_array($field['name'], ['id'])) {
				$data['vrules'] .= $this->getValidationRuleStub($field);
				$data['srules'] .= $this->getSanitizationRuleStub($field);

			}
		}

		return $data;
	}

	protected function getValidationRuleStub($field)
	{
		$rules = [];
		if (isset($field['required'])) {
			$rules[] = "'required'";
		}
		return "\t\t\t'". $field['name'] ."' => [" . implode(',', $rules) . "]," . PHP_EOL;
	}

	protected function getSanitizationRuleStub($field)
	{
		$rules = [];
		if ($field['type'] === 'date') {
			$rules[] = "'dateFromTo:d/m/Y,Y-m-d'";
		}
		return "\t\t\t'". $field['name'] ."' => [" . implode(',', $rules) . "]," . PHP_EOL;
	}

	/**
	 * Get path to the template for the generator
	 *
	 * @return mixed
	 */
	protected function getTemplatePath()
	{
		return $this->getRootTemplatePath() . '/validator.txt';
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['basename', InputArgument::REQUIRED, "The base name (e.g. 'customer address')."]
		];
	}

}
