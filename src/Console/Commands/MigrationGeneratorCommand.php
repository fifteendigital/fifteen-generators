<?php 

namespace Fifteen\Generators\Console\Commands;

use Symfony\Component\Console\Input\InputArgument;

use Fifteen\Generators\FileRenderers\MigrationFileRenderer as FileRenderer;

class MigrationGeneratorCommand extends GeneratorCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gen:migration';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate a migration';

	protected function create($schema = null, $sequence = 0)
	{
		if (!$schema) {
			$schema = $this->schema;
		}
		$migration = new FileRenderer($this->generator);
		$options = $this->options;
		$options['sequence'] = $sequence;

        try {
            $messages = $migration->create($schema, $options);
        } catch (Exception $e) {
            $this->error($e->message);
        }
		foreach ($messages as $message) {
			$this->info($message);
		}
		// Create any child migrations
		foreach ($schema->getChildren() as $child) {
			$this->create($child, ++$sequence);
		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['basename', InputArgument::REQUIRED, "The base name (e.g. 'customer address')."]
		];
	}

}
