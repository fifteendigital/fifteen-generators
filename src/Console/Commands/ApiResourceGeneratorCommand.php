<?php namespace Fifteen\Generators\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ApiResourceGeneratorCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gen:api-resource';
	protected $basename;
	protected $force = false;

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate a new api resource';

	/**
	 * Generate a resource
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->basename = $this->argument('basename');
		$this->force = $this->option('force');

		$this->callTransformer();
		$this->callController();
		$this->callRoutes();

	}

	protected function getMessage($resource, $plurals = false)
	{
		if ($this->option('delete')) {
			$message = "Do you want me to delete the " . $resource . "?";
		} else {
			if ($plurals) {
				$article = '';
			} elseif ($this->beginsWithAVowel($resource)) {
				$article = 'an ';
			} else { 
				$article = 'a ';
			}
			$message = "Do you want me to create " . $article . $resource . "?";
		}
		return $message;
	}

	protected function beginsWithAVowel($word)
	{

		$first_letter = strtolower(substr($word, 0, 1));
		return in_array($first_letter, ['a', 'e', 'i', 'o', 'u']);
	}

	/**
	 * Call model generator if user confirms
	 *
	 * @param $basename
	 */
	protected function callTransformer()
	{
		if ($this->force || $this->confirm($this->getMessage('transformer') . " [yes|no]"))
		{
			$this->call('gen:transformer', $this->getData());
		}
	}

	/**
	 * Call controller generator if user confirms
	 *
	 * @param $basename
	 */
	protected function callController()
	{
		if ($this->force || $this->confirm($this->getMessage('API controller') . " [yes|no]"))
		{
			$this->call('gen:api-controller', $this->getData());
		}
	}

	/**
	 * Call route generator if user confirms
	 *
	 * @param $basename
	 */
	protected function callRoutes()
	{
		if ($this->force || $this->confirm($this->getMessage('API routes', true) . " [yes|no]"))
		{
			$this->call('gen:api-routes', $this->getData());
		}
	}

	public function getData()
	{
		$data = ['basename' => $this->basename];
		foreach (['schema', 'overwrite', 'delete', 'force'] as $item) {
			if ($this->option($item)) {
				$data['--' . $item] = $this->option($item);
			}
		}
		return $data;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['basename', InputArgument::REQUIRED, "The base name (e.g. 'customer address')."]
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['overwrite', null, InputOption::VALUE_REQUIRED, 'Overwrite existing files?'],
			['delete', null, InputOption::VALUE_REQUIRED, 'Delete existing files?'],
			['force', null, InputOption::VALUE_REQUIRED, 'Perform actions without prompting?'],
			['schema', null, InputOption::VALUE_REQUIRED, 'Would you like the file to read from a schema?'],
			['subfolder', null, InputOption::VALUE_REQUIRED, 'Would you like to put the file within a subfolder?'],
			['path', null, InputOption::VALUE_REQUIRED, 'Where should the file be created?'],
			['templatePath', null, InputOption::VALUE_REQUIRED, 'The location of the template for this generator']
		];
	}

}
