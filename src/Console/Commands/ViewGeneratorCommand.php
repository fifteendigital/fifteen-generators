<?php 

namespace Fifteen\Generators\Console\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Fifteen\Generators\FileRenderers\ViewFileRenderer as FileRenderer;


class ViewGeneratorCommand extends \Fifteen\Generators\Console\Commands\GeneratorCommand 
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'gen:view';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate a view';

	/**
	 * Create file(s)
	 *
	 * @return mixed
	 */
	protected function create($schema = null)
	{
		if (!$schema) {
			$schema = $this->schema;
		}
		$renderer = new FileRenderer($this->generator);

        try {
        	$options = $this->options;
        	$options['view'] = $this->argument('view');
        	$messages = [];
        	if ($options['view'] == 'nested') {
				// Create any child files
				foreach ($schema->getChildren() as $child) {
					$this->createNested($child);
				}        		
        	} else {
            	$messages = $renderer->create($schema, $options);
            }
        } catch (Exception $e) {
            $this->error($e->message);
        }
		foreach ($messages as $message) {
			$this->info($message);
		}
		// Create any child files
        foreach ($schema->getChildren() as $child) {
            if (!in_array($options['view'], ['index'])) {
                $this->create($child);
            }
		}
	}

	protected function createNested($schema)
	{
		$renderer = new FileRenderer($this->generator);

        try {
        	$options = $this->options;
        	$options['view'] = 'nested';
            $messages = $renderer->create($schema, $options);
        } catch (Exception $e) {
            $this->error($e->message);
        }
		foreach ($messages as $message) {
			$this->info($message);
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['view', InputArgument::REQUIRED, "The view name (e.g. 'index', 'create' etc.)."],
			['basename', InputArgument::REQUIRED, "The base name (e.g. 'customer address')."]
		];
	}

}
