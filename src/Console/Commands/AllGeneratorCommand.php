<?php 

namespace Fifteen\Generators\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\FileSystem\FileSystem;

class AllGeneratorCommand extends Command 
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gen:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Generate resource for all specified schemas';

    protected $schemas;  

    /**
     * Create a new command instance.
     *
     * @return void
     */
    // public function __construct()
    // {
        
    //     parent::__construct();
    //     $this->setSchemas();
    // }

    protected function setSchemas()
    {
        $schemas = [];
        $filesystem = new FileSystem();
        $file = 'all_generate.json';
        $base_path = base_path('/database/schemas');
        $path = $base_path . '/' . $file;
        if ($filesystem->exists($path)) {
            $contents = $filesystem->get($path);
            $schemas = json_decode($contents);

        } else {
            $this->error("Schema map doesn't exist (all_generate.json)");
            die();
        }
        $this->schemas = $schemas;

    }

    protected function getMessage()
    {

        if ($this->option('delete')) {
            $message = "Are you sure you want to delete all " . count($this->schemas) . " resources?";
        } else {
            $message = "Are you sure you want to create all " . count($this->schemas) . " resources?";
        }
        return $message;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->setSchemas();

        if($this->confirm($this->getMessage() . " [yes|no]")) {

            $schemas = $this->schemas;
            foreach($schemas as $schema) {
                $delete = $this->option('delete');
                if($delete == 'y') {
                    $this->info("Attempting deletion of " . $schema . " resource");
                    \Artisan::call('gen:resource', ['basename' => $schema, '--delete' => $schema, '--force' => 'y']);
                    $this->info($schema . " resource deleted");
                } else {
                    $this->info("Attempting generation of " . $schema . " resource");
                    \Artisan::call('gen:resource', ['basename' => $schema, '--force' => 'y', '--overwrite' => 'y']);
                    $this->info($schema . " resource generated");
                }
                $time = \Carbon\Carbon::now();
                sleep(1); // sleep for a second so migrations named in the correct order
            }
            $this->info("All schemas processed");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('store_split_file_path', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['delete', null, InputOption::VALUE_REQUIRED, 'Delete existing files?']
        ];
    }

}