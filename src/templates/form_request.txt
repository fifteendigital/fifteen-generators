<?php

namespace App\Http\Requests;

use Fifteen\Charabanc\Http\Requests\BaseRequest;

class $STUDLY_SINGULAR$Request extends BaseRequest
{

    /**
     * Sanitization rules
     *
     * @var array
     */
    protected $sanitizer_rules = [
$SRULES$
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
$VRULES$
        ];
    }

}
