
                        <div class="form-group">
                            <label class="col-md-3 control-label">{{ lang('$LANG_APP$.$LABEL$') }}$REQUIRED_STUB$:</label>
                            <div class="col-md-9">
                                {!! Form::textarea('$NAME$', isset($filters['$NAME$']) ? $filters['$NAME$'] : null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
