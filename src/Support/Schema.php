<?php

namespace Fifteen\Generators\Support;

use Illuminate\Support\Str;
use Fifteen\Generators\Support\Field;
use Fifteen\Generators\Support\Relation;

class Schema
{

    public $basename = '';
    protected $json;

    protected $table = '';
    // public $resource = [];
    public $options = [];

    public $fields = [];
    public $relations = [];
    public $children = [];
    public $soft_deletes;

    public $parent;
    protected $timestamps;

    /**
     * @param Generator $generator
     */
    public function __construct($basename, $json, $parent = null)
    {
        $this->basename = $basename;
        $this->json = $json;
        $this->parent = $parent;
        $this->parseSchema($this->json);
    }

    public function parseSchema($items)
    {
        // pd($items);
        // Table name: @TODO - where is this used?
        if ( ! empty($items->table)) {
            $this->table = $items->table;
        } else {
            $this->table = Str::slug($this->basename, '_');
        }

        // // Resource
        // $this->resource = [];
        // if (!empty($items->resource)) {
        //     foreach ($items->resource as $key => $value) {
        //         $this->resource[$key] = $value;
        //     }
        // }
        // $defaults = [
        //     'seed_number' => 50,
        // ];
        // foreach ($defaults as $key => $value) {
        //     if (empty($this->resource[$key])) {
        //         $this->resource[$key] = $value;
        //     }
        // }
        // Options
        $supported_options = [
            'resource_services',
            'seed_number',
            'filters',
            'print',
            'export',
            // 'menu',      // not currently supported
            'icon',
            'permissions',
        ];
        $this->options = [];
        if (!empty($items->options)) {
            foreach ($items->options as $key => $value) {
                if (in_array($key, $supported_options)) {
                    $this->options[$key] = $value;
                }
            }
        }
        $defaults = [
            'seed_number' => 50,
        ];
        foreach ($defaults as $key => $value) {
            if (empty($this->options[$key])) {
                $this->options[$key] = $value;
            }
        }
        // Fields
        $this->fields = [];
        foreach ($items->fields as $name => $item) {
            $field = (array) $item;
            $field['name'] = $name;
            $this->fields[$name] = new Field($field);
        }
        if ( ! empty($this->fields['name'])) {
            $this->fields['name']->required = true;
        }

        // Child resources
        $this->children = [];
        if (!empty($items->children)) {
            foreach ($items->children as $key => $value) {
                // pd($value);
                $this->children[$key] = new Schema($key, $value, $this);
            }
        }

        // Relations
        $this->processRelations($items);
    }

    public function processRelations($items)
    {
        $this->relations = ['hasMany' => [], 'belongsTo' => []];
        if ( ! empty($items->relations)) {
            foreach ($items->relations as $type => $items) {
                foreach ($items as $item) {
                    if (is_object($item)) {
                        $data = (array) $item;
                    } else {
                        $data = ['table' => $item];
                    }
                    $data['type'] = $type;
                    $this->relations[$type][] = new Relation($data);
                }
            }
        }
        $this->processImpliedRelations();
    }

    public function processImpliedRelations()
    {
        if ($this->hasParent()) {
            $type = 'belongsTo';
            $names = $this->parent->getNameStrings();
            $relation = $this->getRelation($type, $names['snake_plural']);
            if (empty($relation)) {
                $data = [
                    'type' => $type,
                    'name' => $names['camel_singular'],
                    'table' => $names['snake_plural'],
                    'foreign_field' => 'id',
                    'local_field' => $names['snake_singular'] . '_id',
                    'on_delete' => 'cascade',
                ];
                $relation = new Relation($data);
                $this->relations[$type][] = $relation;
            }
        }
        $parent_names = $this->getNameStrings();
        foreach ($this->children as $child) {
            $type = 'hasMany';
            $names = $child->getNameStrings();
            $relation = $this->getRelation($type, $names['snake_plural']);
            if (empty($relation)) {
                $data = [
                    'type' => $type,
                    'name' => $names['camel_plural'],
                    'table' => $names['snake_plural'],
                    'foreign_field' => $parent_names['snake_singular'] . '_id',
                    'local_field' => 'id',
                ];
                $relation = new Relation($data);
                $this->relations[$type][] = $relation;
            }
        }
    }

    public function getRelation($type, $table)
    {
        foreach ($this->relations[$type] as $item) {
            if ($item->table == $table) {
                return $item;
            }
        }
    }

    public function getDisplayFields()
    {
        $fields = [];
        foreach ($this->fields as $field) {
            // if ( ! in_array($field->type, ['increments', 'password', 'sequence', 'lookup'])) {
            if ( ! in_array($field->type, ['increments', 'password', 'sequence'])) {            // allow lookups
                $fields[] = $field;
                if (count($fields) >= 3) {
                    break;
                }
            }
        }
        return $fields;
    }

    /**
     * Fetch the field which will be used as the main display field
     * (The first field which isn't an ID field or foreign key will be returned)
     */
    public function getDisplayField()
    {
        if (isset(array_values($this->getDisplayFields())[0]->name)) {
            return array_values($this->getDisplayFields())[0]->name;
        }
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getForeignKeys()
    {
        return $this->relations['belongsTo'];
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function hasFileFields()
    {
        foreach ($this->fields as $field) {
            if (in_array($field->name, ['file', 'image', 'path'])) {
                return true;
            }
        }
    }

    public function getSequenceField()
    {
        foreach ($this->fields as $field) {
            if ($field->type == 'sequence') {
                return $field;
            }
        }
    }

    public function hasSequenceField()
    {
        return !empty($this->getSequenceField());
    }

    public function getOptions($name = null)
    {
        if (empty($name)) {
            return $this->options;
        } elseif (isset($this->options[$name])) {
            return $this->options[$name];
        }
    }
    // public function getResource($name = null)
    // {
    //     if (empty($name)) {
    //         return $this->resource;
    //     } elseif (isset($this->resource[$name])) {
    //         return $this->resource[$name];
    //     }
    // }

    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    public function hasParent()
    {
        return !empty($this->parent->basename);
    }

    public function getRoot()
    {
        if ($this->hasParent()) {
            $schema = $this;
            while ($schema->hasParent()) {
                $schema = $schema->getParent();
            }
            return $schema;
        }
    }

    public function getNameStrings()
    {
        $lang_app = env('LANG_APP', 'app');                         // language file for application strings
        $title_singular = str_singular(ucfirst($this->basename));   // Customer address
        $title_plural = str_plural($title_singular);                // Customer addresses
        $studly_singular = studly_case($title_singular);            // CustomerAddress
        $studly_plural = studly_case($title_plural);                // CustomerAddresses
        $camel_singular = camel_case($title_singular);              // customerAddress
        $camel_plural = camel_case($title_plural);                  // customerAddresses
        $snake_singular = Str::slug($title_singular, '_');          // customer_address
        $snake_plural = Str::slug($title_plural, '_');              // customer_addresses
        $slug_singular = Str::slug($title_singular, '-');           // customer-address
        $slug_plural = Str::slug($title_plural, '-');               // customer-addresses

        return compact('lang_app',
            'title_singular', 'title_plural',
            'studly_singular', 'studly_plural',
            'camel_singular', 'camel_plural',
            'snake_singular', 'snake_plural',
            'slug_singular', 'slug_plural');
    }

}
