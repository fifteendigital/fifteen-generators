<?php 

namespace Fifteen\Generators\Support;

class Relation 
{

    public $type;
    public $name;
    public $table;
    public $model;
    public $foreign_field;
    public $local_field;
    public $on_update;
    public $on_delete;

    public function __construct($data)
    {
        $this->processData($data);
    }

    public function processData($data)
    {
        foreach (['type', 'name', 'table', 'model', 'foreign_field', 'local_field'] as $item) {
            if (isset($data[$item])) {
                $this->$item = $data[$item];
            }
        }
        if (empty($this->name)) {
            if ($this->type == 'hasMany') {
                $this->name = camel_case(str_plural($this->table));
            } elseif ($this->type == 'belongsTo') {
                $this->name = camel_case(str_singular($this->table));
            }
        }
        if (empty($this->model)) {
            $this->model = studly_case(str_singular($this->table));
        }
        if (empty($this->on_update)) {
            $this->on_update = 'cascade';
        }
        if (empty($item->on_delete)) {
            $this->on_delete = 'restrict';
        }
    }

}