<?php

namespace Fifteen\Generators\Support;

use Illuminate\Support\Str;

class Field
{

    public $data_type;
    public $display;
    public $faker;
    public $group_by;
    public $index;
    public $label;
    public $length;
    public $name;
    public $nullable;
    public $required;
    public $table;
    public $type;
    public $validation;

    public function __construct($data)
    {
        $this->processData($data);
    }

    public function processData($data)
    {
        $fields = [
            'data_type',
            'display',
            'faker',
            'group_by',
            'index',
            'label',
            'length',
            'name',
            'nullable',
            'required',
            'table',
            'type',
            'unsigned',
            'validation',
        ];
        $data = (array) $data;
        foreach ($fields as $item) {
            if (isset($data[$item])) {
                $this->$item = $data[$item];
            }
        }
        if ( ! empty($this->validation) && stripos($this->validation, 'required') !== false) {
            $this->required = true;
        }
        if (empty($this->label)) {
            $label = $this->name;
            if (stripos($label, '_id')) {
                $label = substr($label, 0, stripos($label, '_id'));
            }
            $this->label = $label;
        }
        $this->label = Str::slug(labelize($this->label), '_');    // Slug the label again for use with lang files

        if (empty($this->data_type)) {
            switch ($this->type) {
                case 'sequence':
                case 'lookup':
                    $this->data_type = 'integer';
                    $this->unsigned = true;
                    $this->index = true;
                    if (empty($this->table)) {
                        $this->table = Str::slug($this->label, '_');
                    }
                    $this->table_camel = str_plural(Str::camel($this->table));
                    if (empty($this->display)) {
                        $this->display = 'name';
                    }
                    break;
                case 'file':
                case 'image':
                    $this->data_type = 'string';
                    $this->nullable = true;
                    break;
                default:
                    $this->data_type = $this->type;
                    break;
            }
        }
        if (empty($this->faker)) {
            switch ($this->type) {
                case 'increments':
                    break;
                // case 'sequence':
                case 'lookup':
                    $this->faker = '[' . $this->type . ']';   // special items
                    break;
                case 'sequence':
                    $this->faker = 'randomNumber:1';
                    break;
                case 'string':
                    $this->faker = 'company';
                    break;
                case 'text':
                    $this->faker = 'paragraph';
                    break;
                case 'date':
                case 'datetime':
                    $this->faker = 'dateTimeThisDecade';
                    break;
                case 'integer':
                    $this->faker = 'randomNumber';
                    break;
                case 'boolean':
                    $this->faker = 'boolean';
                    break;
            }
        }
    }

}
