<?php

namespace Fifteen\Generators\FileRenderers;

class MigrationFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/migration.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $time = \Carbon\Carbon::now();
        $date = $time->format('Y_m_d');
        $seconds = $time->format('His');
        // increment seconds to ensure the migrations are run in sequence
        if (!empty($options['sequence'])) {
            $seconds += $options['sequence'];
        }
        $filename = $date . '_' . $seconds . '_create_' . $names['snake_plural'] . '_table.php';
        $path = base_path('database/migrations');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema)
    {
        $data = $schema->getNameStrings();

        $data['up'] = $this->getUpStub($schema);
        $data['down'] = $this->getDownStub($schema);

        return $data;
    }

    public function getUpStub($schema)
    {
        $path = $this->getRootTemplatePath() . '/stubs/migration/up.txt';
        $data['table'] = $schema->getTable();
        $data['fields'] = '';
        $data['foreign'] = '';
        foreach ($schema->getFields() as $item) {
            $data['fields'] .= $this->getFieldsStub($item);
        }
        $data['fields'] .= PHP_EOL . "\t\t\t\$table->timestamps();";
        foreach ($schema->getForeignKeys() as $item) {
            $data['foreign'] .= $this->getForeignStub($item);
        }
        $data['fields'] = trim($data['fields']);

        return $this->makeStub($path, $data);
    }

    public function getFieldsStub($item)
    {
        $output = PHP_EOL . "\t\t\t\$table->" . $item->data_type . "('" . $item->name . "'"
            . (empty($item->length) ? '' : ", " . $item->length) . ")";
        if ( ! empty($item->unsigned)) {
            $output .= "->unsigned()";
        }
        if ( ! empty($item->nullable)) {
            $output .= "->nullable()";
        }
        if (isset($item->default)) {
            $output .= "->default('" . $item->default . "')";
        }
        if ( ! empty($item->index)) {
            $output .= "->index()";
        }
        return $output . ';';
    }

    public function getForeignStub($item)
    {
        $output = PHP_EOL . "\t\t\t\$table->foreign('" . $item->local_field . "')"
            . PHP_EOL . "\t\t\t\t  ->references('" . $item->foreign_field . "')"
            . PHP_EOL . "\t\t\t\t  ->on('" . $item->table . "')"
            . PHP_EOL . "\t\t\t\t  ->onUpdate('" . $item->on_update . "')"
            . PHP_EOL . "\t\t\t\t  ->onDelete('" . $item->on_delete . "');";
        return $output;
    }

    public function getDownStub($schema)
    {
        return "Schema::drop('" . $schema->getTable() . "');";
    }


}
