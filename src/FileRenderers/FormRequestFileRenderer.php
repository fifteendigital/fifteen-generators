<?php

namespace Fifteen\Generators\FileRenderers;

class FormRequestFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/form_request.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['studly_singular'] . 'Request.php';
        $path = app_path('Http/Requests');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $data['subfolder'] = $path = empty($options['subfolder']) ? '' : $options['subfolder'];
        $data['namespace'] = empty($data['subfolder']) ? '' : '\\' . $data['subfolder'];
        $data['vrules'] = $data['srules'] = '';
        foreach ($schema->fields as $field) {
            if ( ! in_array($field->name, ['id'])) {
                $data['vrules'] .= $this->getValidationRuleStub($field);
                $data['srules'] .= $this->getSanitizationRuleStub($field);
            }
        }
        return $data;
    }

    protected function getValidationRuleStub($field)
    {
        $rules = [];
        if (isset($field->required)) {
            $rules[] = "'required'";
        }
        return "\t\t\t'". $field->name ."' => [" . implode(',', $rules) . "]," . PHP_EOL;
    }

    protected function getSanitizationRuleStub($field)
    {
        $rules = [];
        if ($field->type === 'date') {
            $rules[] = "'dateFromTo:d/m/Y,Y-m-d'";
        }
        return "\t\t'". $field->name ."' => [" . implode(',', $rules) . "]," . PHP_EOL;
    }

}
