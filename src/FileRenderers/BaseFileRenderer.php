<?php 

namespace Fifteen\Generators\FileRenderers;

use File;

use Fifteen\Generators\Generator;
use Fifteen\Generators\Compilers\TemplateCompiler;

abstract class BaseFileRenderer 
{
    protected $generator;

    /**
     * Fetch the template data
     *
     * @return array
     */
    // protected abstract function getTemplateData();

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    // protected abstract function getFileGenerationPath();

    /**
     * Get the path to the generator template
     *
     * @return mixed
     */
    // protected abstract function getTemplatePath();

    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    public function getRootTemplatePath()
    {
        // TODO: read from config / publish templates to app?
        if(\Config::has('fifteen-generators.template_path'))
        {
            return \Config::get('fifteen-generators.template_path');
        }else{
          return __DIR__ . '/../templates';
        }
    }

    public function makeStub($template_path, $data = [])
    {
        $data = (array) $data;
        return $this->generator->compile($template_path, $data, new TemplateCompiler);
    }

    /**
     * Create the file or files
     *
     * @return mixed
     */
    public function create($schema, $options = [])
    {
        $messages = [];
        $filePathToGenerate = $this->getFileGenerationPath($schema, $options);

        if ( ! File::exists(dirname($filePathToGenerate))) {
            File::makeDirectory(dirname($filePathToGenerate), null, true);
        }

        if (file_exists($filePathToGenerate) && 
            (!empty($options['overwrite']) || !empty($options['delete']))) {
            unlink($filePathToGenerate);
        }
        if (!empty($options['delete'])) {
            $messages[] = "Deleted: {$filePathToGenerate}";
            return $messages;
        }

        try {
            $this->generator->make(
                $this->getTemplatePath($schema, $options),
                $this->getTemplateData($schema, $options),
                $filePathToGenerate
            );
            $messages[] = "Created: {$filePathToGenerate}";
            return $messages;
        } catch (FileAlreadyExists $e) {
            throw new Exception("The file, {$filePathToGenerate}, already exists! I don't want to overwrite it.");
        }
    }

    protected function getRedirectLink($schema, $use_parent_id = false)
    {
        if ($schema->hasParent()) {
            $parent_names = $schema->parent->getNameStrings();
            $link = "'" . $parent_names['slug_plural'] . '.show' . "'";
            if ($use_parent_id) {
                $link .= ", \Request::get('parent_id')";
            } else { 
                $link .= ', $record->' . $parent_names['snake_singular'] . '_id';
            }
        } else {
            $names = $schema->getNameStrings();
            $link = "'" . $names['slug_plural'] . '.index' . "'";
        }
        return $link;
    }

    protected function getShowLink($schema)
    {
        return $this->getShowOrEditLink($schema, 'show');
    }

    protected function getEditLink($schema)
    {
        return $this->getShowOrEditLink($schema, 'edit');
    }

    protected function getShowOrEditLink($schema, $type)
    {
        $names = $schema->getNameStrings();
        $link = "'" . $names['slug_plural'] . '.' . $type . "'";
        if ($schema->hasParent()) {
            $link .= ", [" . '$record->id' . ", 'parent_id' => \Request::get('parent_id')]";
        } else {
            $link .= ", " . '$record->id';
        }
        return $link;
    }

}