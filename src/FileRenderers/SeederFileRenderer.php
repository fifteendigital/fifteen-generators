<?php 

namespace Fifteen\Generators\FileRenderers;

use File;

class SeederFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/table_seeder.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['studly_plural'] . 'TableSeeder.php';
        $path = base_path('database/seeds');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    public function create($schema, $options = [])
    {
        // Create routes file
        $messages = parent::create($schema, $options);
        $data = $schema->getNameStrings();

        // Add to routes.php
        $messages[] = $this->addToModelFactory($schema, $data);

        // Add to navigation
        $messages[] = $this->addToSampleDatabaseSeeder($schema, $data);
        return $messages;
    }

    public function addToModelFactory($schema, $data)
    {
        // Get contents of existing file
        $target_path = base_path('database/factories/ModelFactory.php');
        $contents = File::get($target_path);
        
        // Check it already exists
        $exists = strpos($contents, 'App\Models\\' . $data['studly_singular'] . '::class') !== false;
        if ($exists) {
            return;     // Don't bother
        }

        // Get stub contents
        $path = $this->getRootTemplatePath() . '/stubs/seeders/model_factory.txt';
        $data['fields'] = $this->getFakerFields($schema);
        $stub = $this->makeStub($path, $data);
        
        // Insert into contents
        $position_to_insert = strrpos($contents, ';') + 1;      // Insert after last semicolon (might break the file if it is in a different format to expected)
        $output = substr($contents, 0, $position_to_insert);
        $output .= "\n" . $stub;
        $output .= substr($contents, $position_to_insert);
        
        // Write to file
        File::put($target_path, $output);
        return "Added stub to ModelFactory.php";
    }

    public function getFakerFields($schema)
    {
        $output = '';
        foreach ($schema->fields as $field) {
            if (!empty($field->faker)) {
                $output .= $this->getFakerField($field);
            }
        }
        return $output;
    }
    public function getFakerField($field)
    {
        if (stripos($field->faker, ':') !== false) {
            list($method, $arguments) = explode(':', $field->faker);
            $property = "$method($arguments)";
        } else {
            $property = $field->faker;
        }
        switch ($property) {
            case '[lookup]':
                // $item = '\DB::table(\'' . $field->table . '\')->orderByRaw(\'RAND()\')->pluck(\'id\')';
                $item = '\DB::table(\'' . $field->table . '\')->orderByRaw(\'RAND()\')->first()->id';
                break;
            default:
                $item = '$faker->' . $property;
        }
        return "\n" . str_repeat(' ', 8) . "'" . $field->name . "' => " . $item . ',';
    }

    public function addToSampleDatabaseSeeder($schema, $data)
    {
        // Get contents of existing file
        $target_path = base_path('database/seeds/SampleDatabaseSeeder.php');
        $contents = File::get($target_path);
        
        // Check it already exists
        $exists = strpos($contents, $data['studly_plural'] . 'TableSeeder') !== false;
        if ($exists) {
            return;     // Don't bother
        }
        
        // Add to tables list
        $item = "\n" . str_repeat(' ', 8) . "'" . $schema->getTable() . "',";
        // Insert into contents
        $position_to_insert = strpos($contents, '];') - 5;      // Insert before the end of the tables list (might break the file if it is in a different format to expected)
        $output = substr($contents, 0, $position_to_insert);
        $output .= $item;
        $output .= substr($contents, $position_to_insert);       
        
        $contents = $output; 

        // Get stub contents
        $path = $this->getRootTemplatePath() . '/stubs/seeders/database_seeder.txt';
        $stub = $this->makeStub($path, $data);
        // Insert into contents
        $position_to_insert = strrpos($contents, ';') + 1;      // Insert after last semicolon (might break the file if it is in a different format to expected)
        $output = substr($contents, 0, $position_to_insert);
        $output .= $stub;
        $output .= substr($contents, $position_to_insert);

        // Write to file
        File::put($target_path, $output);
        return "Added stub to SampleDatabaseSeeder.php";
    }



    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $data['seed_number'] = $schema->getOptions('seed_number');

        $data['subfolder'] = $path = empty($options['subfolder']) ? '' : $options['subfolder'];
        $data['namespace'] = empty($subfolder) ? '' : '\\' . $subfolder;

        return $data;
    }

}