<?php 

namespace Fifteen\Generators\FileRenderers;

class TestFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/test.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['studly_plural'] . 'Test.php';
        $path = base_path('tests');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $data['snake_upper_singular'] = strtoupper($data['snake_singular']);

        $data['subfolder'] = $path = empty($options['subfolder']) ? '' : $options['subfolder'];

        $data['namespace'] = empty($data['subfolder']) ? '' : '\\' . $data['subfolder'];

        $data['create'] = $this->getCreateFields($schema, $options);
        $data['edit'] = $this->getEditFields($schema, $options);
        $data['primary_field'] = $schema->getDisplayField();
        return $data;
    }
        
    public function getCreateFields($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $output = '';
        foreach ($schema->fields as $field) {
            $output .= $this->getFieldData($field, 'TEST_CREATE_' . strtoupper($data['snake_singular']) . '_');
        }
        return $output;
    }

    public function getEditFields($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $output = '';
        foreach ($schema->fields as $field) {
            $output .= $this->getFieldData($field, 'TEST_EDIT_' . strtoupper($data['snake_singular']) . '_');
        }
        return $output;
    }

    public function getFieldData($field, $prefix)
    {
        if (in_array($field->name, ['id', 'sequence'])) {
            return;
        }
        switch ($field->type) {
            case 'increments': 
                return;
            case 'boolean':
                $value = 1;
                break;
            case 'date':
                $value = date('d/M/Y');
                break;
            case 'lookup':
                break;
            case 'bigInteger':
            case 'mediumInteger':
            case 'smallInteger':
            case 'tinyInteger':
            case 'decimal':
            case 'double':
            case 'float':
                $value = 10;
                break;
            case 'string':
            case 'text':
            case 'mediumText':
            case 'longText':
            case 'html':
                $value = $prefix . strtoupper($field->name);
                break;
            case 'password':
                break;
            case 'file':
            case 'image':
                break;
            default: 
                break;
        }
        if (isset($value)) {
            return "\n" . str_repeat(' ', 8) . "'" . $field->name . "' => '" . $value . "',";
        }
    }

}