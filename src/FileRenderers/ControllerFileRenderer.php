<?php

namespace Fifteen\Generators\FileRenderers;

use Illuminate\Support\Str;

class ControllerFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath($schema)
    {
        if ($schema->getOptions('resource_services')) {
            return $this->getRootTemplatePath() . '/controller_resource_service.txt';
        }
        return $this->getRootTemplatePath() . '/controller.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['studly_plural'] . 'Controller.php';
        $path = app_path('Http/Controllers');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();

        $data['display_field'] = $schema->getDisplayField();
        $data['subfolder'] = $path = empty($options['subfolder']) ? '' : $options['subfolder'];
        $data['namespace'] = empty($data['subfolder']) ? '' : '\\' . $data['subfolder'];
        $data['viewsfolder'] = empty($subfolder) ? '' : strtolower($subfolder) . '.';
        $index_data_list = ["'dataTable'", "'records'", "'filters'"];
        $create_data_list = ["'defaults'"];
        $show_data_list = ["'record'"];
        $edit_data_list = ["'record'", "'defaults'"];
        $lookup_data_list = [];

        $lookups = $this->getLookupInfo($schema, $options, $lookup_data_list);
        $data['show_nested_records'] = $this->getNestedRecords($schema, $data['slug_plural'] . '.show', $show_data_list);
        $data['edit_nested_records'] = $this->getNestedRecords($schema, $data['slug_plural'] . '.edit', $edit_data_list);
        $data['parent'] = $this->getParent($schema);

        $data += $lookups;

        $index_data_list = array_merge($index_data_list, $lookup_data_list);
        $create_data_list = array_merge($create_data_list, $lookup_data_list);
        $edit_data_list = array_merge($edit_data_list, $lookup_data_list);

        $data['index_data_list'] = implode(', ', $index_data_list);
        $data['create_data_list'] = implode(', ', $create_data_list);
        $data['show_data_list'] = implode(', ', $show_data_list);
        $data['edit_data_list'] = implode(', ', $edit_data_list);

        if (empty($data['create_data_list'])) {
            $data['create_data_list'] = 'null';
        }
        $data['redirect_link'] = $this->getRedirectLink($schema, true);
        $data['defaults'] = $this->getDefaults($schema);
        $data['breadcrumb_root'] = $this->getBreadcrumbRoot($schema);
        return $data;
    }

    public function getBreadcrumbRoot($schema)
    {
        $data = $schema->getNameStrings();
        if ($schema->hasParent()) {
            $path = $this->getRootTemplatePath() . '/stubs/controller/breadcrumb_nested.txt';
            $root_names = $schema->getRoot()->getNameStrings();
            $parent = $schema->getParent();
            $parent_names = $parent->getNameStrings();
            $data['root_slug_plural'] = $root_names['slug_plural'];
            $data['parent_slug_plural'] = $parent_names['slug_plural'];
            $data['root_snake_plural'] = $root_names['snake_plural'];
            $data['parent_snake_plural'] = $parent_names['snake_plural'];
            $data['parent_display_field'] = $parent->getDisplayField();
        } else {
            $path = $this->getRootTemplatePath() . '/stubs/controller/breadcrumb.txt';
        }
        $output = trim($this->makeStub($path, $data));
        return $output;
    }

    public function getParent($schema)
    {
        $data = $schema->getNameStrings();
        $output = '';
        if ($schema->hasParent()) {
            $path = $this->getRootTemplatePath() . '/stubs/controller/parent.txt';
            $parent_names = $schema->getParent()->getNameStrings();
            $data['parent_repo'] = $parent_names['camel_plural'] . 'Repo';
            $output = trim($this->makeStub($path, $data));
        }
        return $output;
    }

    public function getDefaults($schema)
    {
        $path = $this->getRootTemplatePath() . '/stubs/controller/defaults.txt';
        $output = '';
        if ($schema->hasParent()) {
            $parent_names = $schema->parent->getNameStrings();
            $data['name'] = $parent_names['snake_singular'] . '_id';
            $output .= $this->makeStub($path, $data);
        }
        return $output;
    }

    public function getNestedRecords($schema, $route, &$data_list = [])
    {
        $output = '';
        foreach ($schema->getChildren() as $child) {
            $output .= $this->getNestedRecord($child, $route, $data_list);
        }
        return $output;
    }

    public function getNestedRecord($schema, $route, &$data_list = [])
    {
        $path = $this->getRootTemplatePath() . '/stubs/controller/nested_records.txt';
        $data = $schema->getNameStrings();
        $data['route'] = $route;
        $data_list[] = "'" . $data['camel_plural'] . "'";
        $data_list[] = "'" . $data['camel_plural'] . "DataTable'";
        return $this->makeStub($path, $data);
    }

    public function getLookupInfo($schema, $options = [], &$data_list = [])
    {
        $output =  [
            'lookups' => '',
            // 'lookup_list' => '',
            'class_namespaces' => '',
            'property_declarations' => '',
            'injected_classes' => '',
            'property_assignment' => '',
        ];
        $path = $this->getRootTemplatePath() . '/stubs/controller/lookup.txt';
        // dd($schema->getForeignKeys());
        foreach ($schema->getForeignKeys() as $item) {
            $data = [
                'table' => $item->table,
                'table_camel' => camel_case($item->table),
                'repository_class' => studly_case($item->table) . 'Repository',
                'repository_variable' => camel_case($item->table) . 'Repo',
                'primary' => $item->foreign_field,
            ];
            if (in_array("'" . $data['table_camel'] . "'", $data_list)) continue;
            $output['lookups'] .= rtrim($this->makeStub($path, $data), "\n");
            // $output['lookup_list'] .= "'" . $data['table'] . "', ";
            $data_list[] = "'" . $data['table_camel'] . "'";
            $output['class_namespaces'] .= PHP_EOL . "use App\\Repositories\\" . $data['repository_class'] . ';';
            $output['property_declarations'] .= PHP_EOL . "\tprotected $" . $data['repository_variable'] . ';';
            $output['injected_classes'] .= ',' . PHP_EOL . "\t\t" . $data['repository_class'] . " $" . $data['repository_variable'];
            $output['property_assignment'] .= PHP_EOL . "\t\t" . '$this->' . $data['repository_variable'] . " = \$" . $data['repository_variable'] . ';';
        }
        // dd($output);
        $output['lookups'] = trim($output['lookups']);
        // $output['lookup_list'] = trim($output['lookup_list'], ', ');
        $output['class_namespaces'] = trim(str_replace("\t", '    ', $output['class_namespaces']));
        $output['property_declarations'] = trim(str_replace("\t", '    ', $output['property_declarations']));
        $output['injected_classes'] = trim(str_replace("\t", '    ', $output['injected_classes']));
        $output['property_assignment'] = trim(str_replace("\t", '    ', $output['property_assignment']));
        // if (empty($output['lookup_list'])) {
        //     $output['lookup_list'] = 'null';
        // }
        return $output;
    }

}
