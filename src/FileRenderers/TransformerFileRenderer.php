<?php 

namespace Fifteen\Generators\FileRenderers;

use File;
use App\navigation\MainNavigationController;

class TransformerFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/transformer.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $file_name = $names['studly_plural'] . 'Transformer';
        $path = app_path('Transformers');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $file_name . '.php';
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $data['fields'] = $this->getFields($schema);
        return $data;
    }

    public function getFields($schema)
    {
        $output = '';
        foreach ($schema->getFields() as $field) {
            $output .= PHP_EOL . str_repeat(' ', 3 * 4) . "'" . $field->name . "' => \$record['" . $field->name . "'],";
        }
        return trim($output);
    }

}