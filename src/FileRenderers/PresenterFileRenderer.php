<?php

namespace Fifteen\Generators\FileRenderers;

class PresenterFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/presenter.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['studly_singular'] . 'Presenter.php';
        $path = app_path('Presenters');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();

        $data['subfolder'] = $path = empty($options['subfolder']) ? '' : $options['subfolder'];

        $data['namespace'] = empty($data['subfolder']) ? '' : '\\' . $data['subfolder'];

        $data['dates'] = '';

        foreach ($schema->fields as $field) {
            if ($field->type == 'date') {
                $data['dates'] .= $this->getDateStub($field) . "\n";
            }
        }
        $data['dates'] = trim($data['dates']);

        return $data;
    }

    public function getDateStub($field)
    {
        $path = $this->getRootTemplatePath() . '/stubs/presenter/date.txt';
        $data = (array) $field;
        $data['studly_singular'] = studly_case($data['name']);
        return $this->makeStub($path, $data);
    }

}
