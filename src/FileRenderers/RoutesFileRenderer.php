<?php

namespace Fifteen\Generators\FileRenderers;

use File;
use App\Navigation\MainNavigationController;

class RoutesFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/routes.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['snake_plural'] . '.php';
        $path = base_path('routes');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    public function create($schema, $options = [])
    {
        // Create routes file
        $messages = parent::create($schema, $options);
        $data = $schema->getNameStrings();

        // Add to routes.php
        $messages[] = $this->addToRoutes($schema, $data);

        // Add to navigation
        $messages[] = $this->addToNavigation($schema, $data);
        return $messages;
    }

    public function addToRoutes($schema, $data)
    {
        // Get contents of existing file
        $target_path = base_path('routes/auth.php');
        $contents = File::get($target_path);

        // Get stub contents
        $stub = "require_once('" . $data['snake_plural'] . ".php');";

        // Check it already exists
        $exists = strpos($contents, $stub) !== false;
        if ($exists) {
            return;     // Don't bother
        }

        // Append to file
        File::append($target_path, "\n" . $stub);
        return "Added stub to app/Http/routes/auth.php";
    }

    public function addToNavigation($schema, $data)
    {
        if ($schema->hasParent()) {
            return;
        }
        // Get contents of existing file
        $target_path = app_path('Navigation/MainNavigationController.php');
        $contents = File::get($target_path);

        // Get stub contents
        $stub = $this->getNavigationItemsStub($schema, $data);

        // Check it already exists
        $exists = strpos($contents, "'route' => '" . $data['slug_plural'] . ".index'") !== false;
        if ($exists) {
            return;     // Don't bother
        }

        // Insert into contents
        $position_from = stripos($contents, 'public static $items =');
        $position_to = stripos(substr($contents, $position_from), ';') + $position_from;
        $block_start = substr($contents, 0, $position_from);
        $block_end = substr($contents, $position_to);
        $output = $block_start;
        // $output .= "// The following is generated from Fifteen\Generators\Console\Commands\RoutesGeneratorCommand";
        $output .= "\n" . str_repeat(' ', 4) . 'public static $items = ' . $stub;
        $output .= $block_end;

        // Write to file
        File::put($target_path, $output);
        return "Added stub to app/Navigation/MainNavigationController.php";
    }

    public function getNavigationItemsStub($schema, $data)
    {
        $leaf = $this->getNavigationLinkData($schema, $data);
        // pd($link);
        // currently not supported:
        // if (!empty($schema->getOptions('menu')) {
        //     $menu = explode('.', $schema->getOptions('menu')));
        // } else {
        //     $menu = [];
        // }
        $menu = [];
        $items = $this->getNavigationItems($menu);

        $tree = $this->insertAtLocation($items, $menu, $data['title_plural'], $leaf);

        $output = var_export($tree, true);

        // Attempt to format
        $output = str_replace('  ', '    ', $output);
        $output = preg_replace("/\n/", "\n    ", $output);
        $output = preg_replace("#\n(.+)array \(#", "[", $output);
        $output = preg_replace("#array \(#", "[", $output);
        $output = preg_replace("#\)#", "]", $output);
        // dd($output);
        return $output;
    }

    /**
    * Recursive function to add an item to a specific path in the array.
    **/
    public function insertAtLocation($tree, $path, $key, $leaf)
    {
        if (empty($path[0])) {
            $first_submenu = $this->getFirstSubmenu($tree);
            if (!empty($first_submenu)) {
                $position = array_search($first_submenu, array_keys($tree));
                $start = array_slice($tree, 0, $position);
                $start[$key] = $leaf;
                $end = array_slice($tree, $position);
                $tree = $start + $end;
            } else {
                $tree[$key] = $leaf;
            }
        } else {
            $name = $path[0];
            if (isset($tree[$name]['items'])) {
                $items = $tree[$name]['items'];
                array_shift($path);
                $tree[$name]['items'] = $this->insertAtLocation($items, $path, $key, $leaf);
            }
        }
        return $tree;
    }
    public function getFirstSubmenu($tree)
    {
        foreach ($tree as $name => $item) {
            if (!empty($item['items'])) {
                return $name;
            }
        }
    }

    public function getNavigationItems($menu)
    {
        return MainNavigationController::$items;
    }

    public function getNavigationLinkData($schema, $data)
    {
        $item = [
            'route' => $data['slug_plural'] . '.index',
            'lang' => $data['lang_app'] . '.' . $data['snake_plural'],
        ];
        if (!empty($schema->getOptions('icon'))) {
            $item['icon'] = $schema->getOptions('icon');
        }
        if (!empty($schema->getOptions('permissions'))) {
            $item['permissions'] = $schema->getOptions('permissions');
        }
        return $item;
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema)
    {
        $data = $schema->getNameStrings();
        $data['route_path'] = $this->getRoutePath($schema);
        $data['route_name_path'] = $this->getRouteNamePath($schema);
        return $data;
    }

    protected function getRoutePath($schema)
    {
        $names = $schema->getNameStrings();
        return $names['slug_plural'];
    }
    protected function getRouteNamePath($schema)
    {
        $names = $schema->getNameStrings();
        return $names['slug_plural'];
    }

}
