<?php

namespace Fifteen\Generators\FileRenderers;

use Illuminate\Support\Str;

class ViewFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath($schema, $options = [])
    {
        $view_name = $options['view'];

        switch ($view_name) {
            case 'form':
            case 'delete':
            case 'data':
            case 'filters':
                $view_name = 'partials/' . $view_name;
                break;
            case 'nested':
                $view_name = 'nested/index';
                break;
        }
        return $this->getRootTemplatePath() . '/views/' . $view_name . '.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $screen_name = $names['snake_plural'];
        $path = base_path('resources/views');
        $view_name = $options['view'];
        switch ($view_name) {
            case 'form':
            case 'delete':
            case 'data':
            case 'filters':
                $view_name = 'partials/' . $view_name;
                break;
        }
        if ($view_name == 'nested' && !empty($schema->parent->basename)) {
            $parent_names = $schema->parent->getNameStrings();
            $screen_name = $parent_names['snake_plural'] . '/' . $screen_name;
            $view_name = 'index';
        }
        return $path . '/screens/' . $screen_name . '/' . $view_name . '.blade.php';
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $view_name = $options['view'];
        switch ($view_name) {
            case 'data':
                $data['data_fields_1'] = $this->getDataFields($schema, $options, 'odd');
                $data['data_fields_2'] = $this->getDataFields($schema, $options, 'even');
                break;
            case 'form':
                $data['form_fields_1'] = $this->getFormFields($schema, $options, 'odd');
                $data['form_fields_2'] = $this->getFormFields($schema, $options, 'even');
                break;
            case 'sort':
                $data['display_field'] = $schema->getDisplayField();
                $data['redirect_link'] = $this->getRedirectLink($schema, true);
                break;
            case 'index':
            case 'nested':
                $data['sort_link'] = $this->getSortLink($schema, $options);
                $data['column_headers'] = $this->getColumnHeaders($schema, $options);
                $data['column_row'] = $this->getColumnRow($schema, $options);
                $data['print'] = $this->getPrint($schema, $options);
                $data['export'] = $this->getExport($schema, $options);
                $data['filters'] = $schema->getOptions('filters')
                    ? str_repeat(' ', 4) . "@include('screens." . $data['snake_plural'] . ".partials.filters')"
                    : '';
                break;
            case 'edit':
                $data['nested_items'] = $this->getNestedItems($schema);
                $data['enctype'] = $schema->hasFileFields() ? ", 'enctype' => 'multipart/form-data'" : '';
                $data['show_link'] = $this->getShowLink($schema);
                break;
            case 'create':
                $data['enctype'] = $schema->hasFileFields() ? ", 'enctype' => 'multipart/form-data'" : '';
                $data['redirect_link'] = $this->getRedirectLink($schema, true);
                break;
            case 'show':
                $data['nested_items'] = $this->getNestedItems($schema);
                $data['redirect_link'] = $this->getRedirectLink($schema);
                $data['edit_link'] = $this->getEditLink($schema);
                break;
            case 'filters':
                $data['filter_fields_1'] = $this->getFilterFields($schema, $options, 'odd');
                $data['filter_fields_2'] = $this->getFilterFields($schema, $options, 'even');
                break;
        }
        return $data;
    }

    protected function getPrint($schema, $options = [])
    {
        if (!empty($schema->getOptions('print'))) {
            $path = $this->getRootTemplatePath() . '/stubs/views/index/print.txt';
            return $this->makeStub($path);
        }
    }

    protected function getExport($schema, $options = [])
    {
        if (!empty($schema->getOptions('export'))) {
            $path = $this->getRootTemplatePath() . '/stubs/views/index/export.txt';
            $data = $schema->getNameStrings();
            return $this->makeStub($path, $data);
        }
    }

    public function getNestedItems($schema)
    {
        $path = $this->getRootTemplatePath() . '/stubs/views/nested_item.txt';
        $parent_names = $schema->getNameStrings();
        $output = '';
        foreach ($schema->getChildren() as $child) {
            $child_names = $child->getNameStrings();
            $data = [
                'parent' => $parent_names['snake_plural'],
                'child' => $child_names['snake_plural'],
            ];
            $output .= $this->makeStub($path, $data);
        }
        return $output;
    }

    public function getSortLink($schema, $options = [])
    {
        if ($schema->hasSequenceField()) {
            $data = $schema->getNameStrings();
            $data['link'] = "'" . $data['slug_plural'] . '.sort' . "'";
            if ($schema->hasParent()) {
                $parent_names = $schema->parent->getNameStrings();
                $data['link'] .= ', [' . "'group_by' => '" . $parent_names['snake_singular'] . "_id'"
                    . ", 'parent_id' => " . '$parent->id' . ']';
            }
            $path = $this->getRootTemplatePath() . '/stubs/views/index/sort_link.txt';
            return $this->makeStub($path, $data);
        }
    }

    public function getColumnHeaders($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $output = '';
        $display_fields = $schema->getDisplayFields();
        $path = $this->getRootTemplatePath() . '/stubs/views/index/headers';
        if ($schema->hasSequenceField()) {
            $output .= $this->makeStub($path . '/sequence.txt', ['name' => 'sequence']);
        }
        foreach ($display_fields as $field) {
            switch ($field->type) {
                case 'lookup':
                    $file = $path . '/lookup.txt';
                    break;
                case 'boolean':
                    $file = $path . '/checkbox.txt';
                    break;
                default:
                    $file = $path . '/default.txt';
                    break;
            }
            $item = (array) $field;
            $item['lang_app'] = $data['lang_app'];
            if (!empty($item['table'])) {
                $item['table_singular'] = str_singular($item['table']);
            }
            $output .= $this->makeStub($file, $item);
        }
        return trim($output);
    }

    public function getColumnRow($schema, $options = [])
    {
        $data = $schema->getNameStrings();
        $output = '';
        $display_fields = $schema->getDisplayFields();
        $path = $this->getRootTemplatePath() . '/stubs/views/index/cells';
        if ($schema->hasSequenceField()) {
            $output .= $this->makeStub($path . '/sequence.txt', ['name' => 'sequence']);
        }
        foreach ($display_fields as $field) {
            switch ($field->type) {
                case 'lookup':
                    $file = $path . '/lookup.txt';
                    break;
                case 'boolean':
                    $file = $path . '/checkbox.txt';
                    break;
                case 'file':
                    $file = $path . '/file.txt';
                    break;
                case 'image':
                    $file = $path . '/image.txt';
                    break;
                default:
                    $file = $path . '/default.txt';
                    break;
            }
            $item = (array) $field;
            $item['lang_app'] = $data['lang_app'];
            if (!empty($item['table'])) {
                $item['table_singular'] = str_singular($item['table']);
            }
            if ($field->type == 'lookup' && !empty($schema->relations['belongsTo'])) {
                $relation = collect($schema->relations['belongsTo'])->first(function($item) use ($field) {
                    return $field->name == $item->local_field;
                });
                if (empty($relation)) dd($schema->relations['belongsTo'], $field->name, $relation);
                $item['relation'] = empty($relation) ? camel_case($item['table']) : $relation->name;
            }
            $output .= $this->makeStub($file, $item);
        }
        return trim($output);
    }

    public function getDataFields($schema, $options = [], $odd_or_even = 'both')
    {
        $fields = collect($schema->fields)
            ->map(function($field) use ($schema) {
                return $this->getDataStub($schema, $field);
            });
        if ($odd_or_even != 'both') {
            $half = ceil($fields->count() / 2);
            $fields = $fields->chunk($half);
            $fields = $odd_or_even == 'odd' ? $fields->get(0) : $fields->get(1);
        }
        return $fields == null ? '' : $fields->implode('');
    }

    public function getDataFieldsInRows($schema, $options = [], $odd_or_even = 'both')
    {
        // Not used - get in columns instead (getDataFields())
        $fields_string = '';
        $i = 1;
        foreach ($schema->fields as $field) {
            $stub = $this->getDataStub($schema, $field);
            if (!empty($stub) && $this->displayOddOrEven($i++, $odd_or_even)) {
                $fields_string .= $stub;
            }
        }
        return $fields_string;
    }

    protected function displayOddOrEven($i, $odd_or_even = 'both')
    {
        $is_even = $i % 2 == 0;
        switch ($odd_or_even) {
            case 'odd':
                return !$is_even;
            case 'even':
                return $is_even;
        }
        return true;
    }

    public function getDataStub($schema, $field)
    {
        $data = $schema->getNameStrings();
        $path = $this->getRootTemplatePath() . '/stubs/views/data';
        switch ($field->type) {
            case 'increments':
            case 'sequence':
            case 'password':
                return;
            case 'lookup':
                $file = $path . '/lookup.txt';
                break;
            case 'boolean':
                $file = $path . '/checkbox.txt';
                break;
            case 'file':
                $file = $path . '/file.txt';
                break;
            case 'image':
                $file = $path . '/image.txt';
                break;
            default:
                $file = $path . '/text.txt';
                break;
        }
        $item = (array) $field;
        $item['lang_app'] = $data['lang_app'];
        if (!empty($item['table'])) {
            $item['table_singular'] = str_singular($item['table']);
            $item['table_camel_singular'] = str_singular($item['table_camel']);
            $item['relation'] = $item['table_camel_singular'];
        }
        if ($field->type == 'lookup' && !empty($schema->relations['belongsTo'])) {
            $relation = collect($schema->relations['belongsTo'])->first(function($item) use ($field) {
                return $field->name == $item->local_field;
            });
            if (!empty($relation)) {
                $item['relation'] = $relation->name;
            }
        }
        return $this->makeStub($file, $item);
    }

    public function getFormFields($schema, $options = [], $odd_or_even = 'both')
    {
        $templatePath = $this->getRootTemplatePath() . '/stubs/views/form';
        $fields = collect($schema->fields)
            ->map(function($field) use ($schema, $templatePath) {
                return $this->getFormStub($schema, $field, $templatePath);
            });
        if ($odd_or_even != 'both') {
            $half = ceil($fields->count() / 2);
            $fields = $fields->chunk($half);
            $fields = $odd_or_even == 'odd' ? $fields->get(0) : $fields->get(1);
        }
        $fields_string = $fields == null ? '' : $fields->implode('');

        if ($schema->hasParent() && $odd_or_even != 'even') {
            $template_path = $this->getRootTemplatePath() . '/stubs/views/form/parent.txt';
            $fields_string .= $this->makeStub($template_path, []);
        }
        return $fields_string;
    }

    public function getFormFieldsInRows($schema, $options = [], $odd_or_even = 'both')
    {
        $template_path = $this->getRootTemplatePath() . '/stubs/views/form';
        $fields_string = '';
        $i = 1;
        foreach ($schema->fields as $field) {
            $stub = $this->getFormStub($schema, $field,  $template_path);
            if (!empty($stub) && $this->displayOddOrEven($i++, $odd_or_even)) {
                $fields_string .= $stub;
            }
        }
        if ($schema->hasParent() && $odd_or_even != 'even') {
            $template_path = $this->getRootTemplatePath() . '/stubs/views/form/parent.txt';
            $fields_string .= $this->makeStub($template_path, []);
        }
        return $fields_string;
    }

    public function getFilterFields($schema, $options = [], $odd_or_even = 'both')
    {
        $templatePath = $this->getRootTemplatePath() . '/stubs/views/filters';
        $fields = collect($schema->fields)
            ->map(function($field) use ($schema, $templatePath) {
                return $this->getFormStub($schema, $field, $templatePath);
            });
        if ($odd_or_even != 'both') {
            $half = ceil($fields->count() / 2);
            $fields = $fields->chunk($half);
            $fields = $odd_or_even == 'odd' ? $fields->get(0) : $fields->get(1);
        }
        $fields_string = $fields == null ? '' : $fields->implode('');
        return $fields_string;
    }

    public function getFilterFieldsInRows($schema, $options = [], $odd_or_even = 'both')
    {
        $template_path = $this->getRootTemplatePath() . '/stubs/views/filters';
        $fields_string = '';
        $i = 1;
        $ignore_fields = ['file', 'image'];
        foreach ($schema->fields as $field) {
            if (in_array($field->type, $ignore_fields)) {
                continue;
            }
            $stub = $this->getFormStub($schema, $field,  $template_path);
            if (!empty($stub) && $this->displayOddOrEven($i++, $odd_or_even)) {
                $fields_string .= $stub;
            }
        }
        return $fields_string;
    }

    public function getFormStub($schema, $field, $template_path)
    {
        $data = $schema->getNameStrings();
        if (in_array($field->name, ['id', 'sequence'])) {
            return;
        }
        switch ($field->type) {
            case 'increments':
                return;
            case 'boolean':
                $template_path .= '/checkbox.txt';
                break;
            case 'date':
                $template_path .= '/date.txt';
                break;
            case 'lookup':
                $template_path .= '/select.txt';
                break;
            // @TODO: number fields not implemented yet (number.txt is same as text.txt)
            case 'bigInteger':
            case 'mediumInteger':
            case 'smallInteger':
            case 'tinyInteger':
            case 'decimal':
            case 'double':
            case 'float':
                $template_path .= '/number.txt';
                break;
            case 'text':
            case 'mediumText':
            case 'longText':
                $template_path .= '/textarea.txt';
                break;
            case 'html':
                $template_path .= '/textarea.txt';
                break;
            case 'password':
                $template_path .= '/password.txt';
                break;
            case 'file':
                $template_path .= '/file.txt';
                break;
            case 'image':
                $template_path .= '/image.txt';
                break;
            default:
                $template_path .= '/text.txt';
                break;
        }
        $item = (array) $field;
        if (!empty($item['table'])) {
            $item['table_singular'] = str_singular($item['table']);
            $item['table_camel'] = camel_case($item['table']);
        }
        $item['lang_app'] = $data['lang_app'];
        $item['required_stub'] = empty($item['required']) ? '' : '<span class="required">*</span>';
        return $this->makeStub($template_path, $item);
    }


}
