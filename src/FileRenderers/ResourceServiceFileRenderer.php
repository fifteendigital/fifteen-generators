<?php 

namespace Fifteen\Generators\FileRenderers;

class ResourceServiceFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/resource_service.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['studly_plural'] . 'ResourceService.php';
        $path = app_path('ResourceServices');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();

        $data['subfolder'] = $path = empty($options['subfolder']) ? '' : $options['subfolder'];

        $data['namespace'] = empty($data['subfolder']) ? '' : '\\' . $data['subfolder'];

        return $data;
    }

}