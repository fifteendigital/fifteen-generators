<?php 

namespace Fifteen\Generators\FileRenderers;

class RepositoryFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/repository.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['studly_plural'] . 'Repository.php';
        $path = app_path('Repositories');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();

        $data['subfolder'] = $path = empty($options['subfolder']) ? '' : $options['subfolder'];

        $data['namespace'] = empty($data['subfolder']) ? '' : '\\' . $data['subfolder'];

        $data['sortable_fields'] = $this->getSortableFields($schema, $options);

        $data['sortable_fields'] = $this->getSortableFields($schema, $options);

        $data['display_field'] = $schema->getDisplayField();

        $data['sequence'] = $this->getSequence($schema, $options);

        return $data;
    }

    protected function getSortableFields($schema, $options = [])
    {
        $output = '';
        foreach ($schema->fields as $field) {
            $output .= "\n\t\t'" . $field->name . "',";
        }
        return trim($output);
    }

    protected function getSequence($schema, $options = [])
    {
        $sequence_field = $schema->getSequenceField();
        if (empty($sequence_field)) {
            return '';
        }
        $data = ['sequence_field' => $sequence_field->name];
        if (empty($sequence_field->group_by)) {
            $path = $this->getRootTemplatePath() . '/stubs/repository/sequence.txt';
        } else {
            $data['group_by'] = $sequence_field->group_by;
            $path = $this->getRootTemplatePath() . '/stubs/repository/sequence_group_by.txt';
        }
        return $this->makeStub($path, $data);
    }

}