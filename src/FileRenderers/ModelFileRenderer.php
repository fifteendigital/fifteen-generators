<?php

namespace Fifteen\Generators\FileRenderers;

class ModelFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/model.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $filename = $names['studly_singular'] . '.php';
        $path = app_path('Models');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/' . $filename;
    }


    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $names = $schema->getNameStrings();

        $subfolder = $path = empty($options['subfolder']) ? '' : $options['subfolder'];

        $fillable = '';
        $nullable = '';
        $filterable = '';
        $rules = '';
        $set_dates = '';
        $get_dates = '';
        $set_passwords = '';

        foreach ($schema->getFields() as $field) {
            if ( ! in_array($field->name, ['id'])) {
                $fillable .= $this->getFieldStub($field);
                $filterable .= $this->getFieldStub($field);
            }
            if ( ! empty($field->nullable)) {
                $nullable .= $this->getFieldStub($field);
            }
            if ( ! empty($field->required)) {
                $rules .= $this->getRequiredFieldStub($field);
            }
            switch ($field->type) {
                case 'date':
                    $set_dates .= $this->getSetDateStub($field);
                    $get_dates .= $this->getGetDateStub($field);
                    break;
                case 'password':
                    $set_passwords .= $this->getSetPasswordStub($field);
                    break;
            }
        }
        $relations = $this->getRelations($schema, $options);

        $fillable = trim($fillable);
        $nullable = trim($nullable);
        $filterable = trim($filterable);
        $rules = trim($rules);
        $set_dates = trim($set_dates);
        $get_dates = trim($get_dates);

        $soft_deletes = $schema->soft_deletes ? 'use \Illuminate\Database\Eloquent\SoftDeletingTrait;' : '';

        $search = $this->getSearchStub($schema, $options);

        $namespace = empty($subfolder) ? '' : '\\' . $subfolder;
        // $class_namespaces = $this->getClassNamespaces($schema);

        $name = $names['studly_singular'];

        $filters = $this->getFilters($schema);

        $data = compact('name', 'namespace', 'class_namespaces', 'rules', 'fillable', 'filterable', 'nullable', 'soft_deletes',
            'relations', 'search', 'set_dates', 'get_dates', 'set_passwords', 'filters');
        $data['display_field'] = $schema->getDisplayField();

        return $data;
    }

    // public function getClassNamespaces($schema)
    // {
    //     $allRelations = [];
    //     if (!empty($schema->relations['hasMany'])) {
    //         foreach ($schema->relations['hasMany'] as $item) {
    //             if (!in_array($item->model, $allRelations)) $allRelations[] = $item->model;
    //         }
    //     }
    //     if (!empty($schema->relations['belongsTo'])) {
    //         foreach ($schema->relations['belongsTo'] as $item) {
    //             if (!in_array($item->model, $allRelations)) $allRelations[] = $item->model;
    //         }
    //     }
    //     return collect($allRelations)->map(function($item) {
    //             return 'use ' . $item . ';';
    //         })
    //         ->implode(PHP_EOL);
    // }

    public function getFilters($schema)
    {
        $output = '';
        $path = $this->getRootTemplatePath() . '/stubs/model/filter.txt';
        foreach ($schema->getFields() as $item) {
            $data = ['field' => $item->name];
            switch ($item->type) {
                case 'string':
                case 'text':
                    $data['type'] = 'Like';
                    break;
                case 'date':
                    $data['type'] = 'Date';
                    break;
                default:
                    $data['type'] = 'Match';
                    break;
            }
            $output .= $this->makeStub($path, $data);
        }
        return $output;
    }

    public function getFieldStub($field)
    {
        return "\t\t'" . $field->name . "'," . PHP_EOL;
    }

    public function getRequiredFieldStub($field)
    {
        return "\t\t'" . $field->name . "' => 'required'," . PHP_EOL;
    }

    public function getRelations($schema, $options = [])
    {
        $output = '';
        if (!empty($schema->relations['hasMany'])) {
            foreach ($schema->relations['hasMany'] as $item) {
                $output .= "\n" . $this->getRelationStub($item);
            }
        }
        if (!empty($schema->relations['belongsTo'])) {
            foreach ($schema->relations['belongsTo'] as $item) {
                $output .= "\n" . $this->getRelationStub($item);
            }
        }
        return $output;
    }

    public function getRelationStub($item)
    {
        $data = (array) $item;
        $path = $this->getRootTemplatePath() . '/stubs/model/relation.txt';
        $data['model'] = $item->model . '::class';
        $data['optional_arguments'] = '';
        $optional = [];
        if (!empty($item->local_field) && !empty($item->foreign_field)) {     // Need to specify both
            $optional[] = $item->local_field;
            $optional[] = $item->foreign_field;
        }
        if ($item->type == 'hasMany') {
            // arguments are reversed (annoyingly)
            $optional = array_reverse($optional);
        }
        foreach ($optional as $item) {
            $data['optional_arguments'] .= ', \'' . $item . '\'';
        }
        return $this->makeStub($path, $data);
    }


    public function getSetPasswordStub($field)
    {
        $path = $this->getRootTemplatePath() . '/stubs/model/set_password.txt';
        $field->studly_singular = studly_case($field->name);
        return $this->makeStub($path, $field);
    }

    public function getSetDateStub($field)
    {
        $path = $this->getRootTemplatePath() . '/stubs/model/set_date.txt';
        $field->studly_singular = studly_case($field->name);
        return $this->makeStub($path, $field);
    }

    public function getGetDateStub($field)
    {
        $path = $this->getRootTemplatePath() . '/stubs/model/get_date.txt';
        $field->studly_singular = studly_case($field->name);
        return $this->makeStub($path, $field);
    }

    public function getSearchStub($schema, $options = [])
    {
        $table = $schema->getTable();
        if (empty($schema->fields['name'])) {
            $where = "'$table.id', '=', \$search";
        } else {
            $where = "'$table.name', 'LIKE', \"%\$search%\"";
        }
        return "public function scopeSearch(\$query, \$search)" . PHP_EOL
            . "\t{" . PHP_EOL
            . "\t\treturn \$query->where($where);" . PHP_EOL
            . "\t}";
    }

}
