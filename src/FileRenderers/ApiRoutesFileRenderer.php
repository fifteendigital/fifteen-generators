<?php

namespace Fifteen\Generators\FileRenderers;

use File;
use App\navigation\MainNavigationController;

class ApiRoutesFileRenderer extends BaseFileRenderer
{

    /**
     * Get path to the template for the generator
     *
     * @return mixed
     */
    protected function getTemplatePath()
    {
        return $this->getRootTemplatePath() . '/api_routes.txt';
    }

    /**
     * The path where the file will be created
     *
     * @return mixed
     */
    protected function getFileGenerationPath($schema, $options = [])
    {
        $names = $schema->getNameStrings();
        $file_name = $names['snake_plural'];
        // $path = app_path('Http/routes');
        $path = base_path('routes');
        if ( ! empty($options['subfolder'])) {
            $path .= '/' . $options['subfolder'];
        }
        return $path . '/api/' . $file_name . '.php';
    }

    public function create($schema, $options = [])
    {
        // Create routes file
        $messages = parent::create($schema, $options);
        $data = $schema->getNameStrings();

        // Add to routes.php
        $messages[] = $this->addToRoutes($schema, $data);

        return $messages;
    }

    public function addToRoutes($schema, $data)
    {
        // Get contents of existing file
        $target_path = base_path('routes/api.php');
        $contents = File::get($target_path);

        // Get stub contents
        $stub = "require_once('api/" . $data['snake_plural'] . ".php');";

        // Check it already exists
        $exists = strpos($contents, $stub) !== false;
        if ($exists) {
            return;     // Don't bother
        }

        // Append to file
        File::append($target_path, "\n" . $stub);
        return "Added stub to routes/api.php";
    }

    /**
     * Fetch the template data
     *
     * @return array
     */
    protected function getTemplateData($schema, $options = [])
    {
        $data = $schema->getNameStrings();

        return $data;
    }

}
