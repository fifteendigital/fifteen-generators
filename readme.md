#Generators

Designed to work with the Charabanc project, it may be possible to adapt parts of this to work with other Laravel projects in future.

##Installation

Add Generators to your composer.json file:

    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "fifteen/generators": "1.0.*"
    },

Add minimum-stability: dev:
```sh
    "minimum-stability": "dev",
```

As the repository isn't on Packagist, you also need to include it in the repositories list:

```php
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-generators.git"
        }
    ]
```

Run composer update:

```sh
> composer update
```

Register service provider in config/app.php:

```php
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        ...

        /*
         * Package Service Providers...
         */
        \Fifteen\Generators\GeneratorsServiceProvider::class,
    ];
```

Publish templates to your resources folder (optional):

```sh
php artisan vendor:publish --provider="Fifteen\Generators\GeneratorsServiceProvider" --force
```

##Usage

While you are free to create application files from scratch, there are built in generators which may help you to quickly implement boilerplate code, and also reinforce a strong MVC structure, which you are encouraged to follow where possible.

In simple cases, the generators may even allow you to create features without much coding at all; however it is easy to modify the code once generated.

The following generators are available as Artisan commands:

* gen:resource
    * gen:model
    * gen:repository
    * gen:resource-service
    * gen:form-request
    * gen:controller
    * gen:presenter
    * gen:migration
    * gen:seed
    * gen:views
        * gen:view [view]
    * gen:routes
    * gen:transformer
    * gen:test
* gen:api-resource
    * gen:api-controller
    * gen:transformer
    * gen:api-routes
* gen:all

The items listed below gen:resource and gen:api-resource can be run separately; otherwise calling the resource command run all in the list.
gen:view [view] can be run instead of gen-views to generate a view separately (options include index, show, create, edit, form, data, delete).
gen:all reads a file called all_generate.json, and runs gen:resource on a list of resources in the file.

The generators all take a mandatory argument, which is the name of the resource (database table), e.g. 'customer_addresses'.
Optional arguments take the form --option_name=option_value:

* delete: delete existing generated files (take care with this)
* overwrite: overwrite existing generated files (take care with this)
* force: don't prompt for each item
* path
* subfolder
* templatePath

### Example

create two resource schema files in database/schemas:

#### database/schemas/areas.json
```
{
    "fields": {
        "id": {
            "label": "ID",
            "type": "increments"
        },
        "name": {
            "type": "string",
            "length": 50,
            "validation": "required",
            "index": true
        }
    }
}
```

#### database/schemas/companies.json
```
{
    "options": {
        "filters": true,
        "print": true,
        "export": true
    },
    "fields": {
        "id": {
            "label": "ID",
            "type": "increments"
        },
        "area_id": {
            "type": "lookup",
            "table": "areas",
            "display": "name"
        },
        "name": {
            "type": "string",
            "length": 50,
            "validation": "required",
            "index": true
        }
    },
    "relations": {
        "belongsTo": [
            {
                "name": "area",
                "table": "areas",
                "local_field": "area_id",
                "foreign_field": "id"
            }
        ]
    },
    "children": {
        "machines": {
            "fields": {
                "id": {
                    "label": "ID",
                    "type": "increments"
                },
                "company_id": {
                    "type": "lookup",
                    "nullable": true,
                    "table": "companies",
                    "display": "name"
                },
                "name": {
                    "type": "string",
                    "length": 50,
                    "validation": "required",
                    "index": true
                }
            }
        }
    }
}
```

Then run from the command line in your app's root directory:
```sh
$ php artisan gen:resource areas --force=1 --overwrite=1
$ php artisan gen:resource companies --force=1 --overwrite=1
```
 
Run the migrations:
```sh
$ php artisan migrate
```

Seed the database:
```sh
$ composer dump-autoload
$ php artisan db:seed --class=SampleDatabaseSeeder
```

You should then be able to browse your new screens.

##Schema files

### Resource options

There are some options which can be defined in the schema:

* seed_number [50]: number of records to seed
* resource_services [false]: generate a resource service class in between the controller and repository
* filters [false]: generate filter controls on the index screen
* print [false]: add a print button to the index page
* export [false]: add an export button to the index page
* icon [none]: the icon class that appears in the menu (e.g. fa-star)
* permissions [none]: the permissions for accessing the screen (e.g. menu.admin) - please note that this currently only governs what is displayed in the navigation; extra checks need to be put in place in terms of middleware etc. 

### Fields

Fields (or columns) take the format of:

```
    "[field name]": {
        "[option key]": "[option value]",
        ...
    }
```

Supported options:

* type (required): The column type. All of Laravel's schema builder column types are supported (see [Laravel documentation](http://laravel.com/docs/5.1/migrations#creating-columns)). However, there are extended column types, which include:
    * lookup: for lookup fields: (integer, unsigned, index)
    * file (string, nullable)
    * image (string, nullable)
    * sequence (integer, unsigned)
    * html (text)
    * password (string)
* data_type: implied from type, but can be specified separately.
* label: the display name of the field, which will be displayed as the label in a form, column headers in tables etc. If not specified, it will create one based on the field name, e.g. "customer_name" becomes "Customer name".
* length: the field length, e.g. "255" for a string, or "5, 2" for a decimal
* index: set to true if the column is indexed
* nullable: set to true if the column can be set to null
* table: for lookup type columns, the table which is referenced
* display: for lookup type columns, the column to display in the referenced table
* relation
* faker: Faker method to use in the factory file, e.g. "company", "randomNumber:4" etc. If not specified, it will use a faker method suitable for the field type.
* group_by: For sequence columns, specifying a field to group by will manage the sequencing only for rows which have the same value in that field, e.g. lessons can be sequenced within a series, so the series id is specified.
* required: if a column is required, it adds the validation rule
* validation: other validation rules can be specified.

### Relations

* type (required): either hasMany or belongsTo
* table (required): the table that is being referenced
* name: the name which references the relation in the model - if not specified it's the singular of the table name if it's a belongsTo, or plural if it's a hasMany
* model: the model that is being referenced - if not specified it's the singular studly case of the table name
* local_field: the field in the current table
* foreign_field: the field in the table that is being referenced
* on_update [cascade]: restrict, cascade, set null 
* on_delete [restrict]: restrict, cascade, set null

### Nested resources

You can specify nested resources in the 'children' array, which contains the schema data for the nested resource. Relations between the nested resources are created by default but can be specified if required.

## Seeding the database

In order to seed the tables created using the schema, follow the instructions below:

* Locate the SampleDatabaseSeeder and remove defaults from the tables array 
* Ensure that the AreasTableSeeder is ran before CompaniesTableSeeder

Then, to seed the database, run the following command:

```
> php artisan db:seed --class=SampleDatabaseSeeder
```

You may need to run 
```
> composer dump-autoload
```